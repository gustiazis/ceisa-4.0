import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { compose, withProps, lifecycle } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer } from "react-google-maps";
import { QuickActions } from "../../../_metronic/layout/sub-header/quick-actions/QuickActions";
import {
  Portlet,
  PortletBody,
  PortletHeader,
  PortletHeaderToolbar
} from "../../partials/content/Portlet";

const MapWithADirectionsRenderer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD-kec7kuxLbwku7_Pl3Wic9OiAp1JsZqo&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ width: `100%`, height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {
      const DirectionsService = new window.google.maps.DirectionsService();

      DirectionsService.route({
        origin: new window.google.maps.LatLng(-6.1573339, 106.8340436),
        destination: new window.google.maps.LatLng(-6.1566392, 106.84866),
        travelMode: window.google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === window.google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  })
)(props =>
  <GoogleMap
    defaultZoom={12}
    defaultCenter={new window.google.maps.LatLng(-6.1566392, 106.84866)}

  >
    {props.directions && <DirectionsRenderer directions={props.directions} />}
  </GoogleMap>
);

export default function Dashboard() {
  const api = 'https://esbbcext01.beacukai.go.id:8082';
  const [error, setError] = useState(false);
  const [truckings, setTruckings] = useState(null);
  const [pageTruckings, setPageTruckings] = useState(0);
  const [sizeTruckings, setSizeTruckings] = useState(10);
  const [bookedDateTruckings, setBookedDateTruckings] = useState('');
  const [npwpTruckings, setNpwpTruckings] = useState('313022626122000');
  const [detail, setDetail] = useState(null);
  const [data] = useState([{
    orderId: 'PEB - 4214124',
    truckId: 'TRUCK - 437',
    status: 'Validation',
    tracking: 'On Track - 110 KM to IDMRK',
    progress: 25
  }, {
    orderId: 'PEB - 1239237',
    truckId: 'TRUCK - 110',
    status: 'Waiting for Payment',
    tracking: 'On Track - 51 KM to IDTPP',
    progress: 75
  }, {
    orderId: 'PEB - 2451785',
    truckId: 'TRUCK - 326',
    status: 'Document Confirmation',
    tracking: 'On Track - 72 KM to IDTPP',
    progress: 50
  }, {
    orderId: 'PEB - 1237524',
    truckId: 'TRUCK - 89',
    status: 'SPBB - Release',
    tracking: 'Arrived at IDJBK',
    progress: 100
  }, {
    orderId: 'NPE - 4214123',
    truckId: 'TRUCK - 102',
    status: 'Ready to Export',
    tracking: 'Arrived at IDJBK',
    progress: 100
  }]);

  useEffect(() => {
    if (detail) {
      window.scrollTo(0, 0)
    }
  })

  useEffect(() => {
    if (pageTruckings || sizeTruckings || bookedDateTruckings || npwpTruckings) {
      fetchTruckings();
    }
  }, [pageTruckings, sizeTruckings, bookedDateTruckings, npwpTruckings]);

  async function fetchTruckings() {
    const res = await fetch(api + '/DataTransporterDetil/ListActiveTrucks/?page=' + pageTruckings + '&size=' + sizeTruckings + '&bookedDate' + bookedDateTruckings + '=&npwp=' + npwpTruckings);
    res
      .json()
      .then(res => setTruckings(res))
      .catch(err => setError(err));
  }

  const handleProgressBg = (value) => {
    if (isNaN(value)) {
      if (value === 'Arrived Destination') {
        return {
          backgroundColor: 'success',
          percentage: 100
        }
      } else {
        return {
          backgroundColor: 'danger',
          percentage: 25
        }
      }
    } else {
      if (value > 75) {
        return {
          backgroundColor: 'success',
          percentage: value
        }
      } else if (value > 50) {
        return {
          backgroundColor: 'info',
          percentage: value
        }
      } else if (value > 25) {
        return {
          backgroundColor: 'warning',
          percentage: value
        }
      } else {
        return {
          backgroundColor: 'danger',
          percentage: value
        }
      }
    }
  }

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Dashboard</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <button className="btn kt-subheader__btn-secondary">Today</button>
            <button className="btn kt-subheader__btn-secondary">Month</button>
            <button className="btn kt-subheader__btn-secondary">Year</button>
            <button className="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Select dashboard daterange">
              <span className="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title"></span>&nbsp;
              <span className="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Nov 1 - Nov 30</span>
              <i className="flaticon2-calendar-1"></i>
            </button>
            <QuickActions />
          </div>
        </div>
      </div>

      <div className="row">
        <div className={(detail && detail.show === 'less') ? 'col-xl-8' : 'col-xl-12'}>
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletBody fluid={true} className="p-0 overflow-hidden">
              <MapWithADirectionsRenderer />
            </PortletBody>
          </Portlet>
        </div>
        {detail ? (
          <div className={(detail && detail.show === 'less') ? 'col-xl-4' : 'col-xl-12'}>
            <Portlet fluidHeight={true}>
              <PortletHeader
                title={(detail.type === 'truckings' ? 'Trucking' : 'Booking') + ' Details'}
                toolbar={
                  <PortletHeaderToolbar>
                    <button type="button" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent" onClick={() => setDetail({ type: detail.type, show: detail.show === 'less' ? 'more' : 'less', item: detail.item })}>{detail.show === 'less' ? 'More' : 'Less'}</button>
                  </PortletHeaderToolbar>
                }
              />
              {detail.type === 'truckings' ? (
                <div className="kt-portlet__body pt-2">
                  <div className="row">
                    <div className={detail.show === 'more' ? 'col-lg-4' : 'col-lg-12'}>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Order ID"}</span><span className="text-right">{detail.item.idRequestBooking || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Booked Date"}</span><span className="text-right">{detail.item.booked_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"BL No"}</span><span className="text-right">{detail.item.bl_no || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"BL Date"}</span><span className="text-right">{detail.item.bl_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"SP2 Valid Date"}</span><span className="text-right">{detail.item.sp2valid_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"SPC Valid Date"}</span><span className="text-right">{detail.item.spcvalid_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Gross Weight"}</span><span className="text-right">{detail.item.gross_weight || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"URL Tracking"}</span><span className="text-right">{detail.item.urlTracking || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Finished"}</span><span className="text-right">{detail.item.isFinished || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Status"}</span><span className="text-right">{detail.item.status || '-'}</span>
                        </div>
                      </div>
                    </div>
                    {(detail && detail.show === 'more') ? (
                      <div className="col-lg-4">
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"ID Platform"}</span><span className="text-right">{detail.item.id_platform || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Nama Platform"}</span><span className="text-right">{detail.item.nama_platform || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Company"}</span><span className="text-right">{detail.item.company || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Billing Code"}</span><span className="text-right">{detail.item.billingCode || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"POD"}</span><span className="text-right">{detail.item.pod || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Destination"}</span><span className="text-right">{detail.item.destination || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Depo"}</span><span className="text-right">{detail.item.depo || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Total Distance"}</span><span className="text-right">{detail.item.total_distance || '-'} KM</span>
                          </div>
                        </div>
                      </div>
                    ) : null}
                    {(detail && detail.show === 'more') ? (
                      <div className="col-lg-4">
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Container No"}</span><span className="text-right">{detail.item.container_no || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Container Size"}</span><span className="text-right">{detail.item.container_size || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Container Type"}</span><span className="text-right">{detail.item.container_type || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"ID Seal"}</span><span className="text-right">{detail.item.idEseal || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Truck Plate No"}</span><span className="text-right">{detail.item.truckPlateNo || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"Nama Driver"}</span><span className="text-right">{detail.item.namaDriver || '-'}</span>
                          </div>
                        </div>
                        <div className="mt-3">
                          <div className="d-flex justify-content-between mb-1">
                            <span className="kt-font-bold">{"HP Driver"}</span><span className="text-right">{detail.item.hpDriver || '-'}</span>
                          </div>
                        </div>
                      </div>
                    ) : null}
                  </div>
                </div>
              ) : null}
            </Portlet>
          </div>
        ) : null}
        <div className="col-sm-12 col-md-12 col-lg-4">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletHeader
              title={'Active Documents (17)'}
              toolbar={
                <PortletHeaderToolbar>
                  <Link to="/documents" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                </PortletHeaderToolbar>
              }
            />
            <PortletBody>
              {data.map((item, index) => {
                const progress = handleProgressBg(item.progress)

                return (
                  <div key={index} className="mb-3">
                    <div className="d-flex justify-content-between mb-1">
                      <span>{item.orderId}</span><span>{item.status}</span>
                    </div>
                    <div className="progress">
                      <div className={'progress-bar bg-' + progress.backgroundColor} role="progressbar" style={{ width: progress.percentage + "%" }} aria-valuenow={progress.percentage} aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                )
              })}
            </PortletBody>
          </Portlet>
        </div>
        <div className="col-sm-12 col-md-12 col-lg-4">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletHeader
              title={'Active Shipments (10)'}
              toolbar={
                <PortletHeaderToolbar>
                  <Link to="/shipments" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                </PortletHeaderToolbar>
              }
            />
            <PortletBody>
              {data.map((item, index) => {
                const progress = handleProgressBg(item.progress)

                return (
                  <div key={index} className="mb-3">
                    <div className="d-flex justify-content-between mb-1">
                      <span>{(item.truckId).replace('TRUCK', 'SHIP')}</span><span>{item.tracking}</span>
                    </div>
                    <div className="progress">
                      <div className={'progress-bar bg-' + progress.backgroundColor} role="progressbar" style={{ width: progress.percentage + "%" }} aria-valuenow={progress.percentage} aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                )
              })}
            </PortletBody>
          </Portlet>
        </div>
        {truckings ? (
          <div className="col-sm-12 col-md-12 col-lg-4">
            <Portlet className="kt-portlet--border-bottom-brand">
              <PortletHeader
                title={'Active Trucks (' + truckings.totalElements + ')'}
                toolbar={
                  <PortletHeaderToolbar>
                    <Link to="/trucking" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                  </PortletHeaderToolbar>
                }
              />
              <PortletBody>
                {(truckings.content || []).map((item, index) => {
                  if (index < 5) {
                    const progress = handleProgressBg(item.status || '-')

                    return (
                      <div key={index} className="mb-3" style={{ cursor: 'pointer' }} onClick={() => {
                        setDetail({ type: 'truckings', show: 'less', item: item })
                        // handleUpdateMap(item)
                      }}>
                        <div className="d-flex justify-content-between mb-1">
                          <span>{item.nama_platform || '-'} - {item.id_platform || '-'}</span><span>{item.status || '-'}</span>
                        </div>
                        <div className="progress">
                          <div className={'progress-bar bg-' + progress.backgroundColor} role="progressbar" style={{ width: progress.percentage + "%" }} aria-valuenow={progress.percentage} aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                    )
                  }

                  return null
                })}
              </PortletBody>
            </Portlet>
          </div>
        ) : null}
      </div>
    </>
  )
}
