import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Portlet,
  PortletBody
} from "../../partials/content/Portlet";

export default function Shipments() {
  const [shipments] = useState([{
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    }
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    }
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    }
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    }
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    }
  }]);

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Shipments</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <Link to="/shipments/book" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Book New Shipment
            </Link>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletBody fluid={true} className="px-0">
              <div className="table-responsive">
                <table className="table table-lg">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Shipment ID</th>
                      <th>Vessel/Voyage</th>
                      <th>Departure</th>
                      <th>Current Location</th>
                      <th>Arrival</th>
                    </tr>
                  </thead>
                  <tbody>
                    {shipments.map((item, index) => {
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--info'}>
                              <input type="checkbox" disabled/>
                              <span></span>
                            </label>
                          </th>
                          <td>
                            <p className="kt-font-lg kt-font-info mb-1">{item.line}</p>
                            <p>{item.id}</p>
                          </td>
                          <td>
                            <p className="kt-font-lg kt-font-info mb-1">{item.name}</p>
                            <p>{item.vessel} | {item.voyage}</p>
                          </td>
                          <td>
                            <p>{item.departure.location}</p>
                            <p>{item.departure.date}</p>
                          </td>
                          <td>
                            <p>{item.current.location}</p>
                            <p>{item.current.date}</p>
                          </td>
                          <td>
                            <p>{item.arrival.location}</p>
                            <p>{item.arrival.date}</p>
                          </td>
                          <td className="text-right">
                            <button className="btn-label-info btn btn-sm btn-bold btn-action">Details</button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </PortletBody>
          </Portlet>
        </div>
      </div>
    </>
  )
}
