import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Portlet,
  PortletBody,
  PortletHeader,
  PortletHeaderToolbar
} from "../../partials/content/Portlet";

export default function Trucking() {
  const api = 'https://esbbcext01.beacukai.go.id:8082';
  const [error, setError] = useState(false);
  const [tab, setTab] = useState('truckings');
  const [truckings, setTruckings] = useState(null);
  const [pageTruckings, setPageTruckings] = useState(0);
  const [sizeTruckings, setSizeTruckings] = useState(10);
  const [bookedDateTruckings, setBookedDateTruckings] = useState('');
  const [npwpTruckings, setNpwpTruckings] = useState('313022626122000');
  const [bookings, setBookings] = useState(null);
  const [pageBookings, setPageBookings] = useState(0);
  const [sizeBookings, setSizeBookings] = useState(10);
  const [bookedDateBookings, setBookedDateBookings] = useState('');
  const [npwpBookings, setNpwpBookings] = useState('313022626122000');
  const [detail, setDetail] = useState(null);

  useEffect(() => {
    if (detail) {
      window.scrollTo(0, 0)
    }
  })

  useEffect(() => {
    if (pageTruckings || sizeTruckings || bookedDateTruckings || npwpTruckings) {
      fetchTruckings();
    }
  }, [pageTruckings, sizeTruckings, bookedDateTruckings, npwpTruckings]);

  useEffect(() => {
    if (pageBookings || sizeBookings || bookedDateBookings || npwpBookings) {
      fetchBookings();
    }
  }, [pageBookings, sizeBookings, bookedDateBookings, npwpBookings]);

  async function fetchTruckings() {
    const res = await fetch(api + '/DataTransporterDetil/ListActiveTrucks/?page=' + pageTruckings + '&size=' + sizeTruckings + '&bookedDate' + bookedDateTruckings + '=&npwp=' + npwpTruckings);
    res
      .json()
      .then(res => setTruckings(res))
      .catch(err => setError(err));
  }

  async function fetchBookings() {
    const res = await fetch(api + '/DetilBooking/LimitPage/?page=' + pageBookings + '&size=' + sizeBookings + '&bookedDate' + bookedDateBookings + '=&npwp=' + npwpBookings);
    res
      .json()
      .then(res => setBookings(res))
      .catch(err => setError(err));
  }

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Trucking</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <Link to="/trucking/book" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Book New Truck
            </Link>
          </div>
        </div>
      </div>

      <div className="row">
        {detail ? (
          <div className="col-12">
            <Portlet fluidHeight={true}>
              <PortletHeader
                title={(detail.type === 'truckings' ? 'Trucking' : 'Booking') + ' Details'}
                toolbar={
                  <PortletHeaderToolbar onClick={() => setDetail(null)}>
                    <button type="button" className="btn btn-label-danger btn-sm btn-bold btn btn-transparent">Close</button>
                  </PortletHeaderToolbar>
                }
              />
              {detail.type === 'truckings' ? (
                <div className="kt-portlet__body pt-2">
                  <div className="row">
                    <div className="col-lg-4">
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Order ID"}</span><span className="text-right">{detail.item.idRequestBooking || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Booked Date"}</span><span className="text-right">{detail.item.booked_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"BL No"}</span><span className="text-right">{detail.item.bl_no || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"BL Date"}</span><span className="text-right">{detail.item.bl_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"SP2 Valid Date"}</span><span className="text-right">{detail.item.sp2valid_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"SPC Valid Date"}</span><span className="text-right">{detail.item.spcvalid_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Gross Weight"}</span><span className="text-right">{detail.item.gross_weight || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"URL Tracking"}</span><span className="text-right">{detail.item.urlTracking || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Finished"}</span><span className="text-right">{detail.item.isFinished || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Status"}</span><span className="text-right">{detail.item.status || '-'}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"ID Platform"}</span><span className="text-right">{detail.item.id_platform || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Nama Platform"}</span><span className="text-right">{detail.item.nama_platform || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Company"}</span><span className="text-right">{detail.item.company || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Billing Code"}</span><span className="text-right">{detail.item.billingCode || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"POD"}</span><span className="text-right">{detail.item.pod || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Destination"}</span><span className="text-right">{detail.item.destination || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Depo"}</span><span className="text-right">{detail.item.depo || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Total Distance"}</span><span className="text-right">{detail.item.total_distance || '-'} KM</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Container No"}</span><span className="text-right">{detail.item.container_no || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Container Size"}</span><span className="text-right">{detail.item.container_size || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Container Type"}</span><span className="text-right">{detail.item.container_type || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"ID Seal"}</span><span className="text-right">{detail.item.idEseal || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Truck Plate No"}</span><span className="text-right">{detail.item.truckPlateNo || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Nama Driver"}</span><span className="text-right">{detail.item.namaDriver || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"HP Driver"}</span><span className="text-right">{detail.item.hpDriver || '-'}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}
              {detail.type === 'bookings' ? (
                <div className="kt-portlet__body pt-2">
                  <div className="row">
                    <div className="col-lg-4">
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Booking ID"}</span><span className="text-right">{detail.item.idRequestBooking || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Service Order ID"}</span><span className="text-right">{detail.item.idServiceOrder || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Harga Penawaran"}</span><span className="text-right">{detail.item.hargaPenawaran ? ('Rp. ' + (Math.round(detail.item.hargaPenawaran)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')) : '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Paid Status"}</span><span className="text-right">{detail.item.paidStatus ? 'Paid' : 'Unpaid'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Watu Penawaran"}</span><span className="text-right">{detail.item.waktuPenawaran || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Created Date"}</span><span className="text-right">{detail.item.created_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Booked Date"}</span><span className="text-right">{detail.item.booked_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"BL No"}</span><span className="text-right">{detail.item.booking.bl_no || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"BL Date"}</span><span className="text-right">{detail.item.booking.bl_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"SP2 Valid Date"}</span><span className="text-right">{detail.item.booking.sp2valid_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"SPC Valid Date"}</span><span className="text-right">{detail.item.booking.spcvalid_date || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Gross Weight"}</span><span className="text-right">{detail.item.booking.gross_weight || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"URL Tracking"}</span><span className="text-right">{detail.item.urlTracking || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Finished"}</span><span className="text-right">{detail.item.isFinished || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Status"}</span><span className="text-right">{detail.item.status || '-'}</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"ID Platform"}</span><span className="text-right">{detail.item.booking.platform.id_platform || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Nama Platform"}</span><span className="text-right">{detail.item.booking.platform.nama_platform || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Company"}</span><span className="text-right">{detail.item.booking.platform.company || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Billing Code"}</span><span className="text-right">{detail.item.billingCode || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"POD"}</span><span className="text-right">{detail.item.booking.pod || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Destination"}</span><span className="text-right">{detail.item.booking.destination || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Depo"}</span><span className="text-right">{detail.item.booking.depo || '-'}</span>
                        </div>
                      </div>
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"Total Distance"}</span><span className="text-right">{detail.item.booking.total_distance || '-'} KM</span>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4">
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"List Container"}</span>
                        </div>
                      </div>
                      {(detail.item.booking.container || []).map((item) => {
                        return (
                          <div className="mt-3">
                            <div className="d-flex justify-content-between mb-1">
                              <span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span className="kt-font-bold">No</span> : {item.container_no || '-'}, <span className="kt-font-bold">Size </span>: {item.container_size || '-'}, <span className="kt-font-bold">Type</span> : {item.container_type || '-'}</span>
                            </div>
                          </div>
                        )
                      })}
                      <br />
                      <div className="mt-3">
                        <div className="d-flex justify-content-between mb-1">
                          <span className="kt-font-bold">{"List Payment Method"}</span>
                        </div>
                      </div>
                      {(detail.item.payment_method || []).map((item) => {
                        return (
                          <div className="mt-3">
                            <div className="d-flex justify-content-between mb-1">
                              <span><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span className="kt-font-bold">Method</span> : {item.method || '-'}, <span className="kt-font-bold">Channel </span>: {item.channel || '-'}</span>
                            </div>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </div>
              ) : null}
            </Portlet>
          </div>
        ) : null}
        <div className="col-12">
          <div className="kt-portlet kt-portlet--tabs">
            <div className="kt-portlet__head">
              <div className="kt-portlet__head-toolbar col px-0">
                <ul className="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold col px-0" role="tablist">
                  <li className="nav-item col px-0">
                    <a className={'nav-link ' + (tab === 'truckings' ? 'active' : '') + ' col px-0 justify-content-center'} data-toggle="tab" role="tab" onClick={() => {
                        setTab('truckings')
                        setDetail(null)
                      }}>
                      <i className="flaticon2-delivery-truck" aria-hidden="true"></i>Active Trucks
                    </a>
                  </li>
                  <li className="nav-item col px-0">
                    <a className={'nav-link ' + (tab === 'bookings' ? 'active' : '') + ' col px-0 justify-content-center'} data-toggle="tab" role="tab" onClick={() => {
                        setTab('bookings')
                        setDetail(null)
                      }}>
                      <i className="flaticon2-calendar-5" aria-hidden="true"></i>Booking
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="kt-portlet__body p-0">
              <div className="tab-content">
                {tab === 'truckings' && truckings ? (
                  <div className="tab-pane active" role="tabpanel">
                    <div className="kt-form kt-form--label-right">
                      <div className="kt-portlet__body kt-portlet__table__inputs">
                        <div className="form-row">
                          <div className="form-group col-lg-5 row">
                            <label className="col-3 col-form-label">Order ID :</label>
                            <div className="col-9">
                              <div className="kt-input-icon kt-input-icon--right">
                                <input type="text" className="form-control" placeholder="Search..." />
                                <span className="kt-input-icon__icon kt-input-icon__icon--right">
                                  <span><i className="la la-search"></i></span>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-2" />
                          <div className="form-group col-lg-5 row">
                            <label className="col-3 col-form-label">Booked Date :</label>
                            <div className="col-9">
                              <input type="date" className="form-control" value={bookedDateTruckings} onChange={(event) => setBookedDateTruckings(event.target.value)}/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="table-responsive">
                      <table className="table table-lg">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Provider</th>
                            <th>Origin/Destination</th>
                            <th>Container</th>
                            <th>Truck</th>
                          </tr>
                        </thead>
                        <tbody>
                          {(truckings.content || []).map((item, index) => {
                            return (
                              <tr key={index} className={item === (detail && detail.item) ? 'table-row-active' : 'table-row-pointer'} onClick={() => setDetail({ type: 'truckings', item: item })}>
                                <th scope="row">
                                  <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--info'}>
                                    <input type="checkbox" disabled />
                                    <span></span>
                                  </label>
                                </th>
                                <td>
                                  <p className="kt-font-lg kt-font-info mb-1">{item.nama_platform || '-'} - {item.id_platform || '-'}</p>
                                  <p>{item.company || '-'}</p>
                                  <p className="table-text-ellipsis">Order ID : {item.idRequestBooking || '-'}</p>
                                </td>
                                <td>
                                  <p className="kt-font-lg kt-font-info mb-1">{item.pod || '-'}</p>
                                  <p className="table-text-ellipsis">{item.depo || '-'}</p>
                                  <p className="table-text-ellipsis">{item.destination || '-'}</p>
                                </td>
                                <td>
                                  <p>{item.container_no || '-'}</p>
                                  <p>{item.container_size || '-'}</p>
                                  <p>{item.container_type || '-'}</p>
                                </td>
                                <td>
                                  <p>{item.truckPlateNo || '-'}</p>
                                  <p>{item.namaDriver || '-'}</p>
                                  <p>{item.hpDriver || '-'}</p>
                                </td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                    <div className="table-pagination p-4">
                      <div className="kt-pagination  kt-pagination--brand">
                          <ul className="kt-pagination__links">
                              {pageTruckings > 0 ? (
                                <li className="kt-pagination__link--first">
                                    <a onClick={() => setPageTruckings(0)}><i className="fa fa-angle-double-left kt-font-brand"></i></a>
                                </li>
                              ) : null}
                              {pageTruckings > 0 ? (
                                <li className="kt-pagination__link--next">
                                    <a onClick={() => setPageTruckings(pageTruckings - 1)}><i className="fa fa-angle-left kt-font-brand"></i></a>
                                </li>
                              ) : null}
                              <li className="kt-pagination__link--active">
                                  <a onClick={() => false}>{pageTruckings + 1}</a>
                              </li>
                              {pageTruckings < truckings.totalPages - 1 ? (
                                <li className="kt-pagination__link--prev">
                                    <a onClick={() => setPageTruckings(pageTruckings + 1)}><i className="fa fa-angle-right kt-font-brand"></i></a>
                                </li>
                              ) : null}
                              {pageTruckings < truckings.totalPages - 1 ? (
                              <li className="kt-pagination__link--last">
                                    <a onClick={() => setPageTruckings(truckings.totalPages - 1)}><i className="fa fa-angle-double-right kt-font-brand"></i></a>
                                </li>
                              ) : null}
                          </ul>
                          <div className="kt-pagination__toolbar">
                              <select className="form-control kt-font-brand" value={truckings.size} onChange={(event) => setSizeTruckings(event.target.value)}>
                                  <option value="10">10</option>
                                  <option value="20">20</option>
                                  <option value="30">30</option>
                                  <option value="50">50</option>
                                  <option value="100">100</option>
                              </select>
                              <span className="pagination__desc">
                                  Displaying {truckings.numberOfElements} of {truckings.totalElements} records
                              </span>
                          </div>
                      </div>
                    </div>
                  </div>
                ) : null}
                {tab === 'bookings' && bookings ? (
                  <div className="tab-pane active" role="tabpanel">
                    <div className="kt-form kt-form--label-right">
                      <div className="kt-portlet__body kt-portlet__table__inputs">
                        <div className="form-row">
                          <div className="form-group col-lg-5 row">
                            <label className="col-3 col-form-label">Booking ID :</label>
                            <div className="col-9">
                              <div className="kt-input-icon kt-input-icon--right">
                                <input type="text" className="form-control" placeholder="Search..." />
                                <span className="kt-input-icon__icon kt-input-icon__icon--right">
                                  <span><i className="la la-search"></i></span>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className="col-lg-2" />
                          <div className="form-group col-lg-5 row">
                            <label className="col-3 col-form-label">Booked Date :</label>
                            <div className="col-9">
                              <input type="date" className="form-control" value={bookedDateBookings} onChange={(event) => setBookedDateBookings(event.target.value)}/>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="table-responsive">
                      <table className="table table-lg">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Provider</th>
                            <th>Origin/Destination</th>
                            <th>Container</th>
                            <th>Price</th>
                          </tr>
                        </thead>
                        <tbody>
                          {(bookings.content || []).map((item, index) => {
                            return (
                              <tr key={index} className={item === (detail && detail.item) ? 'table-row-active' : 'table-row-pointer'} onClick={() => setDetail({ type: 'bookings', item: item })}>
                                <th scope="row">
                                  <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--info'}>
                                    <input type="checkbox" disabled />
                                    <span></span>
                                  </label>
                                </th>
                                <td>
                                  <p className="kt-font-lg kt-font-info mb-1">{(item.booking && item.booking.platform && item.booking.platform.nama_platform) || '-'} - {(item.booking && item.booking.platform && item.booking.platform.id_platform) || '-'}</p>
                                  <p>{(item.booking && item.booking.platform && item.booking.platform.company) || '-'}</p>
                                  <p className="table-text-ellipsis">Booking ID : {item.idRequestBooking || '-'}</p>
                                </td>
                                <td>
                                  <p className="kt-font-lg kt-font-info mb-1">{(item.booking && item.booking.pod) || '-'}</p>
                                  <p className="table-text-ellipsis">{(item.booking && item.booking.depo) || '-'}</p>
                                  <p className="table-text-ellipsis">{(item.booking && item.booking.destination) || '-'}</p>
                                </td>
                                <td>
                                  <p>{(item.booking && item.booking.container && item.booking.container[0] && item.booking.container[0].container_no) || '-'}</p>
                                  <p>{(item.booking && item.booking.container && item.booking.container[0] && item.booking.container[0].container_size) || '-'}</p>
                                  <p>{(item.booking && item.booking.container && item.booking.container[0] && item.booking.container[0].container_type) || '-'}</p>
                                </td>
                                <td>
                                  <p>{item.hargaPenawaran ? ('Rp. ' + (Math.round(item.hargaPenawaran)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')) : '-'}</p>
                                  <p>{item.paidStatus ? 'Paid' : 'Unpaid'}</p>
                                </td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                    <div className="table-pagination p-4">
                      <div className="kt-pagination  kt-pagination--brand">
                          <ul className="kt-pagination__links">
                              {pageBookings > 0 ? (
                                <li className="kt-pagination__link--first">
                                    <a onClick={() => setPageBookings(0)}><i className="fa fa-angle-double-left kt-font-brand"></i></a>
                                </li>
                              ) : null}
                              {pageBookings > 0 ? (
                                <li className="kt-pagination__link--next">
                                    <a onClick={() => setPageBookings(pageBookings - 1)}><i className="fa fa-angle-left kt-font-brand"></i></a>
                                </li>
                              ) : null}
                              <li className="kt-pagination__link--active">
                                  <a onClick={() => false}>{pageBookings + 1}</a>
                              </li>
                              {pageBookings < bookings.totalPages - 1 ? (
                                <li className="kt-pagination__link--prev">
                                    <a onClick={() => setPageBookings(pageBookings + 1)}><i className="fa fa-angle-right kt-font-brand"></i></a>
                                </li>
                              ) : null}
                              {pageBookings < bookings.totalPages - 1 ? (
                                <li className="kt-pagination__link--last">
                                    <a onClick={() => setPageBookings(bookings.totalPages - 1)}><i className="fa fa-angle-double-right kt-font-brand"></i></a>
                                </li>
                              ) : null}
                          </ul>
                          <div className="kt-pagination__toolbar">
                              <select className="form-control kt-font-brand" value={bookings.size} onChange={(event) => setSizeBookings(event.target.value)}>
                                  <option value="10">10</option>
                                  <option value="20">20</option>
                                  <option value="30">30</option>
                                  <option value="50">50</option>
                                  <option value="100">100</option>
                              </select>
                              <span className="pagination__desc">
                                  Displaying {bookings.numberOfElements} of {bookings.totalElements} records
                              </span>
                          </div>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
