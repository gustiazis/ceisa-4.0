import React, { useState } from "react";
import {
  Portlet,
  PortletBody,
  PortletFooter,
} from "../../partials/content/Portlet";

export default function RequisitionsAdd() {
  const [items] = useState([{
    name: 'Sharpner',
    quantity: '10',
    unit: 'Lusin',
    notes: ''
  }, {
    name: 'Pen',
    quantity: '10',
    unit: 'Lusin',
    notes: ''
  }, {
    name: 'Notebooks',
    quantity: '10',
    unit: 'Lusin',
    notes: ''
  }, {
    name: 'Books',
    quantity: '10',
    unit: 'Lusin',
    notes: ''
  }, {
    name: 'Pencil',
    quantity: '10',
    unit: 'Lusin',
    notes: ''
  }]);

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Requisitions</h3>
            <span className="kt-subheader__separator kt-subheader__separator--v" />
            <span className="kt-subheader__desc">Add New Requisition</span>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-xl-12">
          <Portlet fluidHeight={true}>
            <div className="kt-form kt-form--label-right">
              <PortletBody>
                <div className="form-group row">
                  <div className="col-lg-12">
                    <div className="table-responsive table-responsive-rounded">
                      <table className="table table-sm">
                        <thead className="thead-dark">
                          <tr>
                              <th className="pl-3">Name</th>
                              <th>Quantity</th>
                              <th>Unit</th>
                              <th>Notes</th>
                              <th className="pr-3"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {items.map((item, index) => {
                            return (
                              <tr key={index}>
                                <td className="pl-3">{item.name}</td>
                                <td>
                                  <input type="number" className="form-control" value={item.quantity} />
                                </td>
                                <td>
                                  <select className="form-control" id="unit" value={item.unit}>
                                    <option value={0}>{'Lusin'}</option>
                                    <option value={1}>{'Gross'}</option>
                                    <option value={2}>{'Kodi'}</option>
                                    <option value={3}>{'Rim'}</option>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" className="form-control" value={item.notes} />
                                </td>
                                <td className="pr-3 text-center">
                                  <button type="button" class="btn btn-danger btn-sm btn-icon">
                                    <i class="flaticon2-cross"></i>
                                  </button>
                                </td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-12">
                        <button type="button" className="btn btn-info btn-block">Add Item</button>
                      </div>
                    </div>
                  </div>
                </div>
              </PortletBody>
              <PortletFooter>
                <div className="row">
                  <div className="col-12">
                    <button type="button" className="btn btn-success btn-block">Done</button>
                  </div>
                </div>
              </PortletFooter>
            </div>
          </Portlet>
        </div>
      </div>
    </>
  )
}
