import React from "react";
import { Link } from "react-router-dom";

export default function Billing() {
  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Billing</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <Link to="/billing/add" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Add New Billing
            </Link>
          </div>
        </div>
      </div>
    </>
  )
}
