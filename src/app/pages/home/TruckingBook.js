import React, { useState, useEffect } from "react";
import { compose, withProps, lifecycle } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer } from "react-google-maps";
import {
  Portlet,
  PortletBody,
} from "../../partials/content/Portlet";

const MapWithADirectionsRenderer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyD-kec7kuxLbwku7_Pl3Wic9OiAp1JsZqo&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ width: `100%`, height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {
      const DirectionsService = new window.google.maps.DirectionsService();

      DirectionsService.route({
        origin: new window.google.maps.LatLng(-6.1573339, 106.8340436),
        destination: new window.google.maps.LatLng(-6.1566392, 106.84866),
        travelMode: window.google.maps.TravelMode.DRIVING,
      }, (result, status) => {
        if (status === window.google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result,
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      });
    }
  })
)(props =>
  <GoogleMap
    defaultZoom={12}
    defaultCenter={new window.google.maps.LatLng(-6.1566392, 106.84866)}

  >
    {props.directions && <DirectionsRenderer directions={props.directions} />}
  </GoogleMap>
);

export default function TruckingBook() {
  const [error, setError] = useState(false);
  const [ports, setPorts] = useState([]);
  const [depos, setDepos] = useState([]);
  const [trucks, setTrucks] = useState([]);

  async function fetchPorts() {
    const res = await fetch("https://esbbcext01.beacukai.go.id:8082/Tplaces_port/");
    res
      .json()
      .then(res => setPorts(res))
      .catch(err => setError(err));
  }

  async function fetchDepos() {
    const res = await fetch("https://esbbcext01.beacukai.go.id:8082/Tplaces_empty_depo/");
    res
      .json()
      .then(res => setDepos(res))
      .catch(err => setError(err));
  }

  async function fetchTrucks() {
    const res = await fetch("https://esbbcext01.beacukai.go.id:8082/DetilBooking/idUser/?value=12345");
    res
      .json()
      .then(res => setTrucks(res))
      .catch(err => setError(err));
  }

  const handlePrices = (prefix, price) => {
    return !isNaN(price) && prefix + ' ' + (Math.round(price)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }

  useEffect(() => {
    fetchPorts();
    fetchDepos();
    fetchTrucks();
  }, []);

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Trucking</h3>
            <span className="kt-subheader__separator kt-subheader__separator--v" />
            <span className="kt-subheader__desc">Book New Truck</span>
          </div>
        </div>
      </div>
      {error ? (
        <div role="alert" className="fade alert alert-danger show">{error}</div>
      ) : null}
      <div className="row">
        <div className="col-xl-12">
          <Portlet fluidHeight={true}>
            <div className="kt-form kt-form--label-right">
              <PortletBody>
                <div className="form-row">
                  <div className="form-group col-lg-3">
                    <label>B/L Number :</label>
                    <input type="text" className="form-control" />
                  </div>
                  <div className="form-group col-lg-2">
                    <label className="" htmlFor="from">From :</label>
                    <select className="form-control" id="from">
                      <option value={-1}>{'-'}</option>
                      {ports.map((item, index) => {
                        return (
                          <option key={index} value={item.index}>{item.name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <div className="form-group col-lg-2">
                    <label className="" htmlFor="to">To :</label>
                    <select className="form-control" id="to">
                      <option value={-1}>{'-'}</option>
                      {ports.map((item, index) => {
                        return (
                          <option key={index} value={item.index}>{item.name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <div className="form-group col-lg-2">
                    <label className="" htmlFor="depo">Depo :</label>
                    <select className="form-control" id="depo">
                      <option value={-1}>{'-'}</option>
                      {depos.map((item, index) => {
                        return (
                          <option key={index} value={item.index}>{item.name}</option>
                        )
                      })}
                    </select>
                  </div>
                  <div className="form-group col-lg-3">
                    <label className="" htmlFor="container">Container :</label>
                    <select className="form-control" id="container">
                      <option value={-1}>{'-'}</option>
                      <option value={0}>{'TCNU 1234 - 20 - HQ'}</option>
                      <option value={1}>{'TCNU 3456 - 45 - DRY'}</option>
                      <option value={2}>{'TCNU 5678 - 40 - HQ'}</option>
                    </select>
                  </div>
                </div>
                <div className="form-row">
                  <div className="form-group col-lg-3">
                    <label>Plan Date :</label>
                    <input type="date" className="form-control" />
                  </div>
                  <div className="form-group col-lg-3">
                    <label className="">Booking Date :</label>
                    <input type="date" className="form-control" />
                  </div>
                  <div className="form-group col-lg-2">
                    <label className="">BL Date :</label>
                    <input type="date" className="form-control" />
                  </div>
                  <div className="form-group col-lg-2">
                    <label className="">SP2 Valid Date :</label>
                    <input type="date" className="form-control" />
                  </div>
                  <div className="form-group col-lg-2">
                    <label className="">SPC Valid Date :</label>
                    <input type="date" className="form-control" />
                  </div>
                </div>
                <div className="form-group row mb-0">
                  <div className="col-lg-12">
                    <button type="button" className="btn btn-label-primary btn-bold btn-block"><i className="flaticon2-search-1"></i> Search</button>
                  </div>
                </div>
              </PortletBody>
            </div>
          </Portlet>
        </div>
        <div className="col-xl-6">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletBody fluid={true} className="p-0 overflow-hidden">
              <MapWithADirectionsRenderer />
            </PortletBody>
          </Portlet>
        </div>
        <div className="col-xl-6">
          <Portlet fluidHeight={true}>
            <PortletBody>
              <div className="kt-widget4">
                {trucks.map((item, index) => {
                  return (
                    <div key={index} className="kt-widget4__item">
                      <div className="kt-widget4__icon mr-2">
                        <i className="flaticon2-delivery-truck kt-font-xl kt-font-info"></i>
                      </div>
                      <div className="kt-widget4__info pr-0">
                        <div className="row">
                          <div className="col-5 align-self-center">
                            <p className="kt-widget4__username mb-1">{item.booking.platform.nama_platform || '-'}</p>
                            <p className="kt-widget4__text kt-font-lg kt-font-info">Truck Provider</p>
                          </div>
                          <div className="col-4 align-self-center">
                            <p className="kt-widget4__username mb-1">{item.booking.container_no || '-'}</p>
                            <p className="kt-widget4__text kt-font-lg kt-font-danger">{handlePrices('Rp.', item.hargaPenawaran)}</p>
                          </div>
                          <div className="col-3 align-self-center">
                            <button className="btn-label-info btn btn-sm btn-bold btn-block">View</button>
                            <button className="btn-label-success btn btn-sm btn-bold btn-block">Book</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            </PortletBody>
          </Portlet>
        </div>
      </div>
    </>
  )
}
