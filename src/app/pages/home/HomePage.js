import React, { Suspense, lazy } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Dashboard from "./Dashboard";
import Documents from "./Documents";
import Shipments from "./Shipments";
import ShipmentsBook from "./ShipmentsBook";
import Trucking from "./Trucking";
import TruckingBook from "./TruckingBook";
import Billing from "./Billing";
import Requisitions from "./Requisitions";
import RequisitionsAdd from "./RequisitionsAdd";
import Stock from "./Stock";
import StockAdd from "./StockAdd";
import Employees from "./Employees";
import Report from "./Report";
import Builder from "./Builder";
import DocsPage from "./docs/DocsPage";
import { LayoutSplashScreen } from "../../../_metronic";

const GoogleMaterialPage = lazy(() =>
  import("./google-material/GoogleMaterialPage")
);
const ReactBootstrapPage = lazy(() =>
  import("./react-bootstrap/ReactBootstrapPage")
);

export default function HomePage() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/dashboard" />
        }
        <Route path="/dashboard" component={Dashboard} />
        <Route exact path="/documents" component={Documents} />
        <Route exact path="/shipments" component={Shipments} />
        <Route exact path="/shipments/book" component={ShipmentsBook} />
        <Route exact path="/trucking" component={Trucking} />
        <Route exact path="/trucking/book" component={TruckingBook} />
        <Route exact path="/billing" component={Billing} />
        <Route exact path="/requisitions" component={Requisitions} />
        <Route exact path="/requisitions/add" component={RequisitionsAdd} />
        <Route exact path="/stock" component={Stock} />
        <Route exact path="/stock/add" component={StockAdd} />
        <Route exact path="/employees" component={Employees} />
        <Route exact path="/report" component={Report} />
        <Route path="/builder" component={Builder} />
        <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/docs" component={DocsPage} />
        <Redirect to="/error/error-v1" />
      </Switch>
    </Suspense>
  );
}
