import React, { useState } from "react";
import {
  Portlet,
  PortletBody,
  PortletFooter,
} from "../../partials/content/Portlet";

export default function StockAdd() {
  const [items] = useState([{
    name: 'Sharpner',
    quantity: '10',
    unit: 'Lusin',
    thershold: ''
  }, {
    name: 'Pen',
    quantity: '10',
    unit: 'Lusin',
    thershold: ''
  }, {
    name: 'Notebooks',
    quantity: '10',
    unit: 'Lusin',
    thershold: ''
  }, {
    name: 'Books',
    quantity: '10',
    unit: 'Lusin',
    thershold: ''
  }, {
    name: 'Pencil',
    quantity: '10',
    unit: 'Lusin',
    thershold: ''
  }]);

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Stock</h3>
            <span className="kt-subheader__separator kt-subheader__separator--v" />
            <span className="kt-subheader__desc">Add New Stock</span>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-xl-12">
          <Portlet fluidHeight={true}>
            <div className="kt-form kt-form--label-right">
              <PortletBody>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label">Invoice Number:</label>
                  <div className="col-lg-7">
                    <input type="text" className="form-control" />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label" htmlFor="category">Category:</label>
                  <div className="col-lg-7">
                    <select className="form-control" id="category">
                      <option value={-1}>{'-'}</option>
                      <option value={0}>{'Stationary'}</option>
                      <option value={1}>{'Electronic'}</option>
                      <option value={2}>{'Other'}</option>
                    </select>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label">Items:</label>
                  <div className="col-lg-7">
                    <div className="table-responsive table-responsive-rounded">
                      <table className="table table-sm">
                        <thead className="thead-dark">
                          <tr>
                              <th className="pl-3">Name</th>
                              <th>Quantity</th>
                              <th>Unit</th>
                              <th>Thershold</th>
                              <th className="pr-3"></th>
                          </tr>
                        </thead>
                        <tbody>
                          {items.map((item, index) => {
                            return (
                              <tr key={index}>
                                <td className="pl-3">{item.name}</td>
                                <td>
                                  <input type="number" className="form-control" value={item.quantity} />
                                </td>
                                <td>
                                  <select className="form-control" id="unit" value={item.unit}>
                                    <option value={0}>{'Lusin'}</option>
                                    <option value={1}>{'Gross'}</option>
                                    <option value={2}>{'Kodi'}</option>
                                    <option value={3}>{'Rim'}</option>
                                  </select>
                                </td>
                                <td>
                                  <input type="text" className="form-control" value={item.thershold} />
                                </td>
                                <td className="pr-3 text-center">
                                  <button type="button" class="btn btn-danger btn-sm btn-icon">
                                    <i class="flaticon2-cross"></i>
                                  </button>
                                </td>
                              </tr>
                            )
                          })}
                        </tbody>
                      </table>
                    </div>
                    <button type="button" className="btn btn-info btn-block">Add Item</button>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label" htmlFor="supplier">Supplier:</label>
                  <div className="col-lg-7">
                    <select className="form-control" id="supplier">
                      <option value={-1}>{'-'}</option>
                      <option value={0}>{'Supplier 1'}</option>
                      <option value={1}>{'Supplier 2'}</option>
                      <option value={2}>{'Supplier 3'}</option>
                    </select>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label">Upload Invoice:</label>
                  <div className="col-lg-7">
                    <div className="custom-file">
                      <input type="file" className="custom-file-input" id="upload-invoice" />
                      <label className="custom-file-label" htmlFor="upload-invoice"></label>
                    </div>
                  </div>
                </div>
              </PortletBody>
              <PortletFooter>
                <div className="row">
                  <div className="col-12">
                    <button type="button" className="btn btn-success btn-block">Done</button>
                  </div>
                </div>
              </PortletFooter>
            </div>
          </Portlet>
        </div>
      </div>
    </>
  )
}
