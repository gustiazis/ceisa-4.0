import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Portlet,
  PortletBody
} from "../../partials/content/Portlet";

export default function Employees() {
  const [employees] = useState([{
    name: 'Grant Marshall ',
    position: 'Junior Software Engineer',
    depatment: 'IT',
    lastOrdered: '2 Aug 2019',
    joined: '2 Aug 2019',
    mobileNo: '01718332233'
  }, {
    name: 'Pena Veldez ',
    position: 'Executive',
    depatment: 'Marketing',
    lastOrdered: '2 Aug 2019',
    joined: '2 Aug 2019',
    mobileNo: '09112312343'
  }, {
    name: 'Jessica Miles ',
    position: 'Intern',
    depatment: 'Marketing',
    lastOrdered: '2 Aug 2019',
    joined: '2 Aug 2019',
    mobileNo: '01812223123'
  }, {
    name: 'Kerri Barber ',
    position: 'Junior Analyst',
    depatment: 'IT',
    lastOrdered: '2 Aug 2019',
    joined: '2 Aug 2019',
    mobileNo: '01900000123'
  }, {
    name: 'Natasha Gamble ',
    position: 'Software Engineer',
    depatment: 'IT',
    lastOrdered: '2 Aug 2019',
    joined: '2 Aug 2019',
    mobileNo: '01982122222'
  }]);

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Employees</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <Link to="/employees/add" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Add New Employee
            </Link>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletBody fluid={true} className="px-0">
              <div className="table-responsive">
                <table className="table table-md">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Department</th>
                      <th>Last Ordered</th>
                      <th>Joined</th>
                      <th>Mobile No</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {employees.map((item, index) => {
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <img className="rounded" width="30px" src={'/media/users/300_' + (index + 1) + '.jpg'} alt={item.name} />
                          </th>
                          <td>{item.name}</td>
                          <td>{item.position}</td>
                          <td>{item.depatment}</td>
                          <td>{item.lastOrdered}</td>
                          <td>{item.joined}</td>
                          <td>{item.mobileNo}</td>
                          <td className="text-right">
                            <button className="btn-label-warning btn btn-sm btn-bold btn-action">Edit</button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </PortletBody>
          </Portlet>
        </div>
      </div>
    </>
  )
}
