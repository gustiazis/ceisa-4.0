import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  Portlet,
  PortletBody
} from "../../partials/content/Portlet";

export default function Stock() {
  const [stocks] = useState([{
    name: 'A4 Size Paper',
    remaining: '30',
    status: 'High'
  }, {
    name: 'Brush',
    remaining: '29',
    status: 'High'
  }, {
    name: 'Mug',
    remaining: '5',
    status: 'Low'
  }, {
    name: 'Gift Box ',
    remaining: '6',
    status: 'Low'
  }, {
    name: 'HDMI Cable',
    remaining: '3',
    status: 'Low'
  }, {
    name: 'Mobile Charger',
    remaining: '33',
    status: 'High'
  }, {
    name: 'Multiplug',
    remaining: '20',
    status: 'Medium'
  }, {
    name: 'Mouse',
    remaining: '15',
    status: 'Medium'
  }, {
    name: 'Bulb',
    remaining: '2',
    status: 'Low'
  }, {
    name: 'Visiting Card',
    remaining: '0',
    status: 'Empty'
  }]);

  const handleStatusText = (value) => {
    if (value === 'High') {
      return "success"
    } else if (value === 'Medium') {
      return "info"
    } else if (value === 'Low') {
      return "warning"
    } else {
      return "danger"
    }
  }

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Stock</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <Link to="/stock/add" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Add New Stock
            </Link>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletBody fluid={true} className="px-0">
              <div className="table-responsive">
                <table className="table table-md">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Remaining</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {stocks.map((item, index) => {
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--info'}>
                              <input type="checkbox" disabled/>
                              <span></span>
                            </label>
                          </th>
                          <td>{item.name}</td>
                          <td>{item.remaining}</td>
                          <td><p className={'kt-font-bold kt-font-' + handleStatusText(item.status)}>{item.status}</p></td>
                          <td className="text-right">
                            <button className="btn-label-success btn btn-sm btn-bold btn-action-xl">Send Requisitions</button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </PortletBody>
          </Portlet>
        </div>
      </div>
    </>
  )
}
