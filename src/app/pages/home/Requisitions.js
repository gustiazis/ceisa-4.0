import React, { useState } from "react";
import { Link } from "react-router-dom";
import { QuickActions } from "../../../_metronic/layout/sub-header/quick-actions/QuickActions";
import SalesBarChart from "../../../app/widgets/SalesBarChart";
import {
  Portlet,
  PortletBody,
  PortletHeader,
  PortletHeaderToolbar
} from "../../partials/content/Portlet";

export default function Requisitions() {
  const [requisitions] = useState([{
    name: 'Grant Marshall ',
    position: 'Junior Software Engineer',
    depatment: 'IT',
    mobileNo: '01718332233'
  }, {
    name: 'Pena Veldez ',
    position: 'Executive',
    depatment: 'Marketing',
    mobileNo: '09112312343'
  }, {
    name: 'Jessica Miles ',
    position: 'Intern',
    depatment: 'Marketing',
    mobileNo: '01812223123'
  }, {
    name: 'Kerri Barber ',
    position: 'Junior Analyst',
    depatment: 'IT',
    mobileNo: '01900000123'
  }, {
    name: 'Natasha Gamble ',
    position: 'Software Engineer',
    depatment: 'IT',
    mobileNo: '01982122222'
  }]);

  const [stocks] = useState([{
    name: 'Tissue Paper',
    progress: 25,
    remaining: 5
  }, {
    name: 'A4 Size Paper',
    progress: 50,
    remaining: 20
  }, {
    name: 'Mug',
    progress: 75,
    remaining: 30
  }]);

  const handleProgressBg = (value) => {
    if (value > 75) {
      return "success"
    } else if (value > 50) {
      return "info"
    } else if (value > 25) {
      return "warning"
    } else {
      return "danger"
    }
  }

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Requisitions</h3>
          </div>
          <div className="kt-subheader__toolbar">
            <Link to="/requisitions/add" className="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
              Add New Requisition
            </Link>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-12">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletHeader
              title={'Requisitions (5)'}
              toolbar={
                <PortletHeaderToolbar>
                  <Link to="/requisitions" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                </PortletHeaderToolbar>
              }
            />
            <PortletBody fluid={true} className="px-0">
              <div className="table-responsive">
                <table className="table table-md">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Position</th>
                      <th>Department</th>
                      <th>Mobile No</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {requisitions.map((item, index) => {
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <img className="rounded" width="30px" src={'/media/users/300_' + (index + 1) + '.jpg'} alt={item.name} />
                          </th>
                          <td>{item.name}</td>
                          <td>{item.position}</td>
                          <td>{item.depatment}</td>
                          <td>{item.mobileNo}</td>
                          <td className="text-right">
                            <button className="btn-label-info btn btn-sm btn-bold btn-action">Details</button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </PortletBody>
          </Portlet>
        </div>
        <div className="col-sm-12 col-md-12 col-lg-6">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletHeader
              title={'Stock (3)'}
              toolbar={
                <PortletHeaderToolbar>
                  <Link to="/stock" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent">See All</Link>
                </PortletHeaderToolbar>
              }
            />
            <PortletBody>
              {stocks.map((item, index) => {
                return (
                  <div key={index} className="mb-3">
                    <div className="d-flex justify-content-between mb-1">
                      <span>{item.name}</span><span>{item.remaining || 0} Remaining</span>
                    </div>
                    <div className="progress">
                      <div className={'progress-bar bg-' + handleProgressBg(item.progress)} role="progressbar" style={{ width: item.progress + "%" }} aria-valuenow={item.progress} aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                )
              })}
            </PortletBody>
          </Portlet>
        </div>
        <div className="col-sm-12 col-md-12 col-lg-6">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletHeader
              title={'Usage (16)'}
              toolbar={
                <PortletHeaderToolbar>
                  <Link to="/" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent dropdown-toggle">Stationary</Link>
                  &nbsp;&nbsp;&nbsp;
                  <Link to="/" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent dropdown-toggle">From</Link>
                  &nbsp;&nbsp;&nbsp;
                  <Link to="/" className="btn btn-label-primary btn-sm btn-bold btn btn-transparent dropdown-toggle">To</Link>
                </PortletHeaderToolbar>
              }
            />
            <PortletBody>
              <SalesBarChart />
            </PortletBody>
          </Portlet>
        </div>
      </div>
    </>
  )
}
