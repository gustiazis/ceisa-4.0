import React, { useState } from "react";
import {
  Portlet,
  PortletBody,
  PortletHeader
} from "../../partials/content/Portlet";

export default function ShipmentsBook() {
  const [shipments] = useState([{
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    },
    transit: '7 Stops',
    duration: '30'
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    },
    transit: '7 Stops',
    duration: '30'
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    },
    transit: '7 Stops',
    duration: '30'
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    },
    transit: '7 Stops',
    duration: '30'
  }, {
    line: 'Maerks Line',
    id: 'SHIP-76320',
    name: 'Zim Dalian',
    vessel: '0685',
    voyage: '068N',
    departure: {
      location: 'Jakarta - UTC-1 Terminal',
      date: '20 Sep 2019 05:00'
    },
    current: {
      location: 'Tanjung Pelepas',
      date: '23 Sep 2019 05:00'
    },
    arrival: {
      location: 'Hamburg, Germany',
      date: '1 Oct 2019 15:00'
    },
    transit: '7 Stops',
    duration: '30'
  }]);

  return (
    <>
      <div id="kt_subheader" className="kt-subheader kt-subheader-in kt-grid__item">
        <div className="kt-container kt-container--fluid">
          <div className="kt-subheader__main">
            <h3 className="kt-subheader__title">Shipments</h3>
            <span className="kt-subheader__separator kt-subheader__separator--v" />
            <span className="kt-subheader__desc">Book New Shipment</span>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-xl-12">
          <Portlet fluidHeight={true}>
            <div className="kt-form kt-form--label-right">
              <PortletHeader title="Search Vessels" />
              <PortletBody>
                <div className="form-row">
                  <div className="form-group col-lg-4">
                    <label>From :</label>
                    <input type="text" className="form-control" />
                  </div>
                  <div className="form-group col-lg-4">
                    <label>To :</label>
                    <input type="text" className="form-control" />
                  </div>
                  <div className="form-group col-lg-4">
                    <label>Departure Date :</label>
                    <input type="date" className="form-control" />
                  </div>
                </div>
                <div className="form-group row mb-0">
                  <div className="col-lg-12">
                    <button type="button" className="btn btn-label-primary btn-bold btn-block"><i className="flaticon2-search-1"></i> Search</button>
                  </div>
                </div>
              </PortletBody>
            </div>
          </Portlet>
        </div>
        <div className="col-12">
          <Portlet className="kt-portlet--border-bottom-brand">
            <PortletHeader title="Availalbe Vessels" />
            <PortletBody fluid={true} className="px-0">
              <div className="table-responsive">
                <table className="table table-lg">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Shipment ID</th>
                      <th>Vessel/Voyage</th>
                      <th>Departure</th>
                      <th>Arrival</th>
                      <th>Transit & Duration</th>
                    </tr>
                  </thead>
                  <tbody>
                    {shipments.map((item, index) => {
                      return (
                        <tr key={index}>
                          <th scope="row">
                            <label className={'kt-checkbox kt-checkbox--solid kt-checkbox--info'}>
                              <input type="checkbox" disabled/>
                              <span></span>
                            </label>
                          </th>
                          <td>
                            <p className="kt-font-lg kt-font-info mb-1">{item.line}</p>
                            <p>{item.id}</p>
                          </td>
                          <td>
                            <p className="kt-font-lg kt-font-info mb-1">{item.name}</p>
                            <p>{item.vessel} | {item.voyage}</p>
                          </td>
                          <td>
                            <p>{item.departure.location}</p>
                            <p>{item.departure.date}</p>
                          </td>
                          <td>
                            <p>{item.arrival.location}</p>
                            <p>{item.arrival.date}</p>
                          </td>
                          <td>
                            <p className="kt-font-lg kt-font-danger">{item.transit}, {item.duration}</p>
                          </td>
                          <td className="text-right">
                            <button className="btn-label-info btn btn-sm btn-bold btn-action">Details</button>
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            </PortletBody>
          </Portlet>
        </div>
      </div>
    </>
  )
}
